package com.confspot.harvest.twitter

import groovy.util.GroovyTestCase
import org.junit.Assert

class TestStreamingTwitterHarvester extends GroovyTestCase {
	public void testRestart() {
		StreamingTwitterHarvester s = new StreamingTwitterHarvester()
		s.restart(['#grails48', 'justin bieber'])

		System.sleep(5000)
	}
}
