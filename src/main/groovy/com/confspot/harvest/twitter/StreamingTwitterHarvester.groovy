package com.confspot.harvest.twitter

import groovy.util.logging.Log4j

import twitter4j.*

@Log4j
public class StreamingTwitterHarvester {

	// Clase soporte para el acceso a Twitter
	private Support support

	// Referencia a los objetos con los que interactuaremos con Twitter
	List twitters

	// List of the streams used to harvest the tweets
	List twitterStreams

	public void restart(List<String> hashtags, Closure processor = {this.process(it)}) {
		stop()
		start(hashtags, processor)
	}

	// It stops the harvesting of tweets
	private void stop() {
		log.debug("Stop of the streaming twitter harvester")
		if(twitterStreams) { 
			twitterStreams*.cleanUp()
			twitterStreams*.shutdown()
		}
	}

	// It starts to listening for the hashtags passed as parameter
	private void start(List<String> hashtags, Closure processor) {
		log.debug("Starting the streaming twitter harvester")
	
		if(!support) {
			support = new Support()
			support.init()
		}

		if(!twitters && support){
			twitters = support.connectBatchTwitter()
			//twitters = [support.connectTwitterInteractive()]
		}

		
		if(!support || !twitters) {
			log.warn("No se ha realizado la conexión con las cuentas de Twitter")
		} else {
			listen(hashtags, processor)
		}
	}

	/**
	 * Abre las conexiones a Twitter, para buscar tweets empleando los tracks asociados a los
	 * hashtags pasados como parámetros. Por cada tweet recibido ejecuta la closure pasada.
	 */
	private void listen(List<String> hashtags, Closure processor) {
                def listener = [:]
              
		listener.onStatus = { tweet -> 
                        log.info("Mensaje recibido: ${tweet.user.name} + ${tweet.text}")
			processor(tweet)
                }

                listener.onDeletionNotice = { statusDeletionNotice -> log.info("statusDeletionNotice: ${statusDeletionNotice}") }
                listener.onTrackLimitationNotice = { numberOfLimitedStatuses -> log.info("numberOfLimitedStatuses: ${numberOfLimitedStatuses}") }
                listener.onException = { exception -> log.info("Excepción en StreamingTwitterHarvester: ${exception.printStackTrace()}") }
        
		listen(listener as StatusListener, hashtags)
        }

	/**
	 * Enlaza el listener pasado como parámetro con las conexiones a Twitter que se abrirán
	 * para buscar por los hashtags pasados.
	 */
	private void listen(StatusListener listener, List<String> hashtags) {
		def filter, subTracks, twitterStream

		twitterStreams = []
		twitters.eachWithIndex { twitter, index ->
			twitterStream = new TwitterStreamFactory(twitter.configuration).getInstance()
			twitterStreams << twitterStream
			filter = new FilterQuery()

			twitterStream.addListener(listener)

			subTracks = getTracks(hashtags, index, twitters.size())

			log.debug("Tracks para la conexión ${index} (${subTracks.size()}): ${subTracks}")

        	        filter.track(subTracks)
                	twitterStream.filter(filter)
		}
	}

	/**
	 * Agrega de forma sincronizada un tweet a la lista de tweets recolectados
	 */
	public void process(Object item) {
		log.debug('Default tweet processing...')
	}

	/**
	 * Devuelve la sublista de elementos de tracks asociada al grupo currentIndex, teniendo en cuenta que existen
	 * numGroups grupos.
	 */
	String[] getTracks(List<String> tracks, int currentIndex, int numGroups) {
		return subList(tracks, currentIndex, numGroups) as String[]
	}

	/**
	 * Devuelve la sublista de list, posicionada en el índice 'currentIndex' (empezando en cero),
	 * considerando que hay numGroups sublistas.
	 */
	private List subList(List list, int currentIndex, int numGroups) {
		Integer numElements = Math.ceil(list.size() / (numGroups as double)) as int
		Integer min = currentIndex * numElements
		Integer max = Math.min(list.size(), min + numElements) - 1

		return list[min..max]
	}

	public static void main(String[] args) {
		StreamingTwitterHarvester s = new StreamingTwitterHarvester()
		//s.restart(["banco interamericano de desarrollo", "programa de agua potable", "autoridad del agua", bahia panama proyecto OR "saneamiento rural de panama" OR "saneamiento indigena de panama" OR "infraestructura educativa en panama"])
		s.restart(['#RealGirlsEatCakeMusicVideo'])

		Thread.sleep(5000)
	}
}
